package fr.ajc.spring.abalea.eshop;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import model.Article;
import model.Avis;
import repo.ArticleRepository;
import repo.AvisRepository;


@RestController
@RequestMapping("/api/articles")
public class RestArticleController {
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@Autowired
	private AvisRepository avisRepository;
	
	@PostMapping
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void create(Article article, BindingResult result, HttpServletResponse httpResponse) throws IOException {
		
		if (result.hasErrors()) {
			System.out.println("L'article n'a pas été ajouté.");
			httpResponse.sendRedirect("/ajout");
			
		} else {
			articleRepository.save(article);
			httpResponse.sendRedirect("/");
		}
	    
	}
	
	@PostMapping("/article")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public void createAvis(Avis avis, BindingResult result, HttpServletResponse httpResponse) throws IOException {
		System.out.println("Commentaire :"+avis.getCommentaire());
		System.out.println("Id :"+avis.getId_article());
		System.out.println("Note :"+avis.getNote());

		if (result.hasErrors()) {
			System.out.println("L'avis n'a pas été ajouté.");
			httpResponse.sendRedirect("/");
			
		} else {
			avisRepository.save(avis);
			httpResponse.sendRedirect("/");
		}
	    
	}
	
	
}
	


