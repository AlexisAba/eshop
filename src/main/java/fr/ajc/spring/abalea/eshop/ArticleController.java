package fr.ajc.spring.abalea.eshop;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import model.Article;
import model.Avis;
import model.Categorie;
import repo.ArticleRepository;
import repo.AvisRepository;
import repo.CategorieRepository;


@Controller
public class ArticleController implements WebMvcConfigurer{
	
	@Autowired
	private ArticleRepository articleRepository;
	
	@Autowired
	private CategorieRepository categorieRepository;
	
	@Autowired
	private AvisRepository avisRepository;
	
	@Value("${spring.application.name}")
	String appName;
	
	@GetMapping("/")
	public String homePage(Model model) {
	
	model.addAttribute("appName", appName);
	Iterable<Article> findAllArticle = articleRepository.findAll();
	
	model.addAttribute("allArticle", findAllArticle);
	return "home";
	}
	
	
	@GetMapping("/ajout")
	public String ajout(Model model) {
		model.addAttribute("appName", appName);
		Iterable<Categorie> listCategorie = categorieRepository.findAll();
		
		model.addAttribute("allCategorie", listCategorie);
		return "ajout";
	}
	
	
	@GetMapping("/recherche")
	public String recherche(Model model, @PathParam(value = "article") String article) {
		model.addAttribute("appName", appName);
		
		try {
			Iterable<Article> listSearch = articleRepository.findByNomContainingAllIgnoreCase(article);
			
			model.addAttribute("listSearch", listSearch);
		} catch(Exception ex) {
			
		}
		
		
		return "recherche";
		
	}
	
	@GetMapping("/topArticle")
	public String topArticle(Model model) {
		model.addAttribute("appName", appName);
		
		
		return "topArticle";
	}
	
	@GetMapping("/article")
	public String article(Model model, @PathParam(value = "id") long id) {
		model.addAttribute("appName", appName);
		
		Article article = articleRepository.findById(id).get();
		
		model.addAttribute("article",article);
		return "article";
	}
	 
	
	
	

//*********************   Fonction de generation  *********************************************
	
	@GetMapping("/generate")
	public void generate(Model model, HttpServletResponse httpResponse) throws IOException {
		List<Article> listArticle = generateArticle();
		articleRepository.saveAll(listArticle);
		
		List<Categorie> listCategorie = generateCategorie();
		categorieRepository.saveAll(listCategorie);
		
		//List<Avis> listAvis = generateAvis();
		//avisRepository.saveAll(listAvis);
		httpResponse.sendRedirect("/");
	}
	
	/*
	public static List<Avis> generateAvis(){
		
		List<Avis> avis = new ArrayList<Avis>();
		
		avis.add(new Avis(2, "Test commentaire 1",));
		avis.add(new Avis(5, "Test commentaire 2"));
		avis.add(new Avis(1, "Test commentaire 3"));
		avis.add(new Avis(4, "Test commentaire 4"));
		avis.add(new Avis(3, "Test commentaire 5"));
		
		return avis;
	}
	*/
	public static List<Categorie> generateCategorie(){
		
		List<Categorie> categories = new ArrayList<Categorie>();
		categories.add(new Categorie("Sous-vêtement"));
		categories.add(new Categorie("Vêtement de jour"));
		categories.add(new Categorie("Vêtement de nuit"));
		categories.add(new Categorie("Vêtement été"));
		categories.add(new Categorie("Vêtement hiver"));
		categories.add(new Categorie("Chaussure"));
		return categories;
		
	}
	
	public static List<Article> generateArticle() {
		String marques[] = {"G-Star","Levis", "Pepe Jean", "Esprit", "Lidl", "Lacoste", "Tommy Hilfiger", "Monoprix", "Carrefour", "Jules", "Wish", "Celio"};
		String vetements[] = {"Jean", "Pull", "Chemise", "Chaussette", "T-shirt", "Manteau", "Boxer", "Gilet","Slip", "String", "Echarpe", "Basket"};
		String tailles[] = {"XS", "S", "M","L","XL","XXL"};
		List<Article> articles = new ArrayList<Article>();
		
		for(int i = 0;i < 20; i++) {
			Random r = new Random();
			int indiceM = r.nextInt(11);
			int indiceV = r.nextInt(11);
			int indiceT = r.nextInt(5);
			

			Random p = new Random();
			int prix = 1 + p.nextInt(250 - 1);
		
			String nameArticle = vetements[indiceV]+" "+marques[indiceM]+" "+tailles[indiceT];
			
			 articles.add(new Article(nameArticle, prix));
		}
		return articles;
	}
}
