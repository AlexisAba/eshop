package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.Article;


public interface ArticleRepository extends CrudRepository<Article, Long>{
	List<Article> findByNomContainingAllIgnoreCase(String name);
}
