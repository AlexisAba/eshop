package repo;

import org.springframework.data.repository.CrudRepository;

import model.Avis;

public interface AvisRepository extends CrudRepository<Avis, Long>{

}
