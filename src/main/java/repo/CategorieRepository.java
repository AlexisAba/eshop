package repo;

import org.springframework.data.repository.CrudRepository;

import model.Article;
import model.Categorie;

public interface CategorieRepository extends CrudRepository<Categorie, Long>{

}
